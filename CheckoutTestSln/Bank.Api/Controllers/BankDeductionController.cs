﻿using Bank.ServiceInterface.Interfaces;
using Bank.ServiceInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Bank.Api.Controllers
{
    public class BankDeductionController : ApiController
    {

        readonly IBankServicesInterface _bankServices;
        public BankDeductionController(IBankServicesInterface bankServices)
        {
            _bankServices = bankServices;
        }
        // GET: api/BankDeduction
        public BankResponseModel Get()
        {
            return new BankResponseModel();
        }

        // POST: api/BankDeduction
        public BankResponseModel Post(TransactionRequestModel value)
        {
            var result = _bankServices.ProcessBankDeduction(value.cardNumber, value.ccv, value.amount,value.expiryDate);

            return result;
        }


    }
}
