﻿using Bank.ServiceInterface.Interfaces;
using Bank.ServiceInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Bank.Api.Controllers
{
    public class BankMockProcessController : ApiController
    {
        IBankServicesInterface _bankServices;
        public BankMockProcessController(IBankServicesInterface bankServices)
        {
            _bankServices = bankServices;
        }
        // GET api/BankMockProcess
        public BankResponseModel Get()
        {
            var result = _bankServices.GetBankresponse("1", 786, 1200.50,DateTime.Today);
            return result;
        }

        // POST api/BankMockProcess
        public void Post([FromBody]string value)
        {

        }
    }
}
