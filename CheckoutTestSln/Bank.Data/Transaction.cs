//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bank.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Transaction
    {
        public System.Guid Id { get; set; }
        public System.Guid account_id { get; set; }
        public System.DateTime time_stamp { get; set; }
        public string description { get; set; }
        public Nullable<double> amount { get; set; }
        public Nullable<double> PriorBalance { get; set; }
        public Nullable<double> PostBalance { get; set; }
    
        public virtual Account Account { get; set; }
    }
}
