﻿using Bank.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Repositories
{
    public class CustomerRepo
    {
        private BankMockupEntities1 _db;

        public CustomerRepo(BankMockupEntities1 db)
        {
            _db = db;
        }

        public Customer GetCustomerFromCardNum(string cardNumber)
        {
            var result = new Customer();
            try
            {
                var cardObj = _db.Cards.Where(i => i.Card_number == cardNumber).SingleOrDefault();
                var accountCard = cardObj.Account_Card.Single();
                result = accountCard.Customer_AccountCard.Single().Customer;

            }
            catch (Exception ex)
            {
                //new Logger().AddLogMessage(ex);
            }

            return result;
        }

    }
}
