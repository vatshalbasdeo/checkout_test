﻿using Bank.Data;
using Bank.ServiceInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Repositories
{
    public class TransactionRepo
    {
        private BankMockupEntities1 _db;

        public TransactionRepo(BankMockupEntities1 db)
        {
            _db = db;
        }

        public CustomerVerificationModel VerfiyCustomer(string cardNumber,decimal ccv, double amount, DateTime expiryDate)
        {
            bool sufficientBalance = true;
            try
            {
                var cardObj = _db.Cards.Where(i => i.Card_number == cardNumber && i.ccv==ccv).SingleOrDefault();
                var account = cardObj.Account_Card.Single().Account;
                var accountBalance = cardObj.Account_Card.Single().Account.balance;

                var dateOk = cardObj.expiry_date == expiryDate;
                if (dateOk)
                {
                    if (Convert.ToDouble(accountBalance) < amount)
                    {
                        sufficientBalance = false;
                    }
                    else
                        sufficientBalance = true;

                    return new CustomerVerificationModel { SufficientFund = sufficientBalance, AccountId = account.Id };
                }
               
            }
            catch (Exception ex)
            {
                //new Logger().AddLogMessage(ex);
            }
            return new CustomerVerificationModel { SufficientFund = false };

        }

        public BankResponseModel ProcessDeductionOfAmount(Guid accountId, double amount)
        {
            try
            {
                var account = _db.Accounts.Where(i => i.Id == accountId).Single();

                var startingBalance = account.balance;
                var closingBalance = startingBalance - amount;

                var newTrax = new Transaction {Id= Guid.NewGuid(),
                    account_id = accountId,
                    amount = amount,
                    PriorBalance =startingBalance.Value,
                    PostBalance =closingBalance.Value,
                    time_stamp =DateTime.Now};

                var trax = _db.Transactions.Add(newTrax);

                account.balance = closingBalance;

                _db.SaveChanges();
                return new BankResponseModel { Status = true, Guid = newTrax.Id };
            }
            catch (Exception)
            {

                throw;
            }
            return new BankResponseModel { Status = false };
        }

        public BankResponseModel ProcessMerchantPayable(Guid merchantId, double amount)
        {
            try
            { 
                var account = _db.Accounts.Where(i => i.Id == merchantId).SingleOrDefault();

                var startingBalance = account.balance;
                var closingBalance = startingBalance + amount;

                var newTrax = new Transaction {
                    Id = Guid.NewGuid(),
                    account_id = account.Id,
                    amount = amount,
                    PriorBalance = startingBalance.Value,
                    PostBalance = closingBalance.Value,
                    time_stamp = DateTime.Now };

                var trax = _db.Transactions.Add(newTrax);

                account.balance = closingBalance;

                _db.SaveChanges();
                return new BankResponseModel { Status=true,Guid=newTrax.Id};
            }
            catch (Exception)
            {

                throw;
            }
            return new BankResponseModel { Status = false};
        }

    }
}
