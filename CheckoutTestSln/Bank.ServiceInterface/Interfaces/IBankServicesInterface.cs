﻿using Bank.ServiceInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.ServiceInterface.Interfaces
{
    public interface IBankServicesInterface
    {
        BankResponseModel GetBankresponse(string cardNumber, decimal ccv, double amount, DateTime expiryDate);

        BankResponseModel ProcessBankDeduction(string cardNumber, decimal ccv, double amount, DateTime expiryDate);

        BankResponseModel ProcessBankDeposit(string cardNumber, decimal ccv, double amount, DateTime expiryDate);


    }
}
