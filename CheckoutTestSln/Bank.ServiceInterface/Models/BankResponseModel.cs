﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.ServiceInterface.Models
{
    public class BankResponseModel
    {
        public BankResponseModel()
        {
            Status = false;
            Guid = Guid.Empty;
        }
        public Guid Guid { get; set; }
        public bool Status { get; set; }

    }
}
