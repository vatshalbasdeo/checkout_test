﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.ServiceInterface.Models
{
    public class TransactionRequestModel
    {
        public string cardNumber { get; set; }
        public decimal ccv { get; set; }
        public double amount { get; set; }
        public DateTime expiryDate { get; set; }
    }
}
