﻿using Bank.Data;
using Bank.Repositories;
using Bank.ServiceInterface.Interfaces;
using Bank.ServiceInterface.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Services
{
    public class BankServices : IBankServicesInterface
    {
        readonly TransactionRepo bankTransaction;
        public BankServices(BankMockupEntities1 db)
        {
            bankTransaction = new TransactionRepo(db);
        }

        public BankResponseModel GetBankresponse(string cardNumber, decimal ccv, double amount, DateTime expiryDate)
        {
            var processCustomer = new BankResponseModel();
            var processMerchant = new BankResponseModel();
            if (amount > 0)
            {
                var customerAccount = bankTransaction.VerfiyCustomer(cardNumber, ccv, amount,expiryDate);
                if (customerAccount.SufficientFund && customerAccount.AccountId != Guid.Empty)
                {
                    processCustomer = bankTransaction.ProcessDeductionOfAmount(customerAccount.AccountId, amount);
                    //processMerchant = bankTransaction.ProcessMerchantPayable(MerchantId, amount);
                }
                if (processCustomer.Status && processMerchant.Status)
                {
                    return new BankResponseModel { Status = true };
                }

            }

            return new BankResponseModel { Status = false };
        }

        public BankResponseModel ProcessBankDeduction(string cardNumber, decimal ccv, double amount, DateTime expiryDate)
        {
            var result = new BankResponseModel();
            //verify customer 
            var customerStatus = bankTransaction.VerfiyCustomer(cardNumber, ccv, amount, expiryDate);
            if (customerStatus.SufficientFund && customerStatus.AccountId!=Guid.Empty)
            {
                // process deduction
                var trasactionStatus = bankTransaction.ProcessDeductionOfAmount(customerStatus.AccountId, amount);
                if (trasactionStatus.Status)
                {
                    result = trasactionStatus;
                }
            }

            return result;
        }

        public BankResponseModel ProcessBankDeposit(string cardNumber, decimal ccv, double amount, DateTime expiryDate)
        {
            var result = new BankResponseModel();
            if (expiryDate <= DateTime.Today) { return result; }
            
            //verify customer 
            var customerVerification = bankTransaction.VerfiyCustomer(cardNumber, ccv, amount, expiryDate);
            if (customerVerification.SufficientFund && customerVerification.AccountId != Guid.Empty)
            {
                // process deduction
                var trasactionStatus = bankTransaction.ProcessMerchantPayable(customerVerification.AccountId, amount);
                if (trasactionStatus.Status)
                {
                    result = trasactionStatus;
                }
            }

            return result;
        }
    }
}
