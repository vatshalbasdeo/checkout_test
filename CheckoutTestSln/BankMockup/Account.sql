﻿CREATE TABLE [dbo].[Account]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [balance] NUMERIC(20) NULL, 
    [currency] UNIQUEIDENTIFIER NULL, 
    [account_number] VARCHAR(18) NULL 
)
