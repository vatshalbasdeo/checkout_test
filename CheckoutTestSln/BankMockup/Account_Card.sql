﻿CREATE TABLE [dbo].[Account_Card]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [account_id] UNIQUEIDENTIFIER NOT NULL, 
    [card_id] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [FK_Accout_Card_To_Account] FOREIGN KEY (account_id) REFERENCES [Account](Id), 
    CONSTRAINT [FK_Accout_Card_To_Card] FOREIGN KEY (card_id) REFERENCES [Cards](Id)
)
