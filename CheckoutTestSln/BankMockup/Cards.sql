﻿CREATE TABLE [dbo].[Cards]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [Card_number] VARCHAR(18) NULL, 
    [ccv] NUMERIC(3) NULL, 
    [issue_date] DATETIME NULL, 
    [expiry_date] DATETIME NULL, 
    [account_Id] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [FK_Cards_To_Account] FOREIGN KEY ([account_Id]) REFERENCES [Account]([Id]) 
)
