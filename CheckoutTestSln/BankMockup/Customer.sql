﻿CREATE TABLE [dbo].[Customer]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [Last_name] NCHAR(10) NOT NULL, 
    [First_name] NCHAR(10) NOT NULL, 
    [Street] NCHAR(10) NULL, 
    [City] NCHAR(10) NULL, 
    [Country] NCHAR(10) NULL, 
    [Phone] NCHAR(10) NULL, 
    [Email] NCHAR(10) NULL
)
