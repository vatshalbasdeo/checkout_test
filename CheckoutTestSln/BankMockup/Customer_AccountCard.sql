﻿CREATE TABLE [dbo].[Customer_AccountCard]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [customer_id] UNIQUEIDENTIFIER NOT NULL, 
    [accountCard_id] UNIQUEIDENTIFIER NOT NULL, 
    CONSTRAINT [FK_Customer_AccountCard__AccountCard] FOREIGN KEY ([accountCard_id]) REFERENCES [Account_Card](Id), 
    CONSTRAINT [FK_Customer_AccountCard_To_Customer] FOREIGN KEY ([customer_id]) REFERENCES [Customer](Id)
)
