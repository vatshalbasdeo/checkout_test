﻿CREATE TABLE [dbo].[Transactions]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [account_id] UNIQUEIDENTIFIER NOT NULL, 
    [time_stamp] DATETIME NOT NULL, 
    [amount] NUMERIC NOT NULL, 
    [PriorBalance] NUMERIC NOT NULL, 
    [PostBalance] NUMERIC NOT NULL, 
    [description] NVARCHAR(MAX) NULL, 
    CONSTRAINT [FK_Transactions_To_Account] FOREIGN KEY (account_id) REFERENCES [Account](Id)
)
