﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Merchant.DAL
{
    public class ProductModel
    {
        public Guid Id { get; set; }
        public string  ProductName { get; set; }
        public string  ProductDescription { get; set; }
        public double ProductPrice { get; set; }

    }
}
