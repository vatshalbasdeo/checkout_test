﻿using Merchant.DAL;
using Merchant.ShopWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Merchant.ShopWeb.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        MerchantDbEntities _db;
        public HomeController()
        {
            _db = new MerchantDbEntities();
        }
        public ActionResult Index()
        {
            var listOfProducts = _db.Products.Select(i => new ProductModel {
                Id = i.Id,
                ProductName = i.ProductName,
                ProductDescription = i.ProductDescription,
                ProductPrice = i.ProductPrice??0
            }).ToList();

            var vm = new HomeViewModel { ProductModels=listOfProducts};
            return View(vm);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}