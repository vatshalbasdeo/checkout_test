﻿using Merchant.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Merchant.ShopWeb.Models
{
    public class HomeViewModel
    {
        public List<ProductModel> ProductModels { get; set; }
    }
}