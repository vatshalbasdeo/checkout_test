﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Merchant.ShopWeb.Startup))]
namespace Merchant.ShopWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
