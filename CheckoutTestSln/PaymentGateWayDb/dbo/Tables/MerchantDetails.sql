﻿CREATE TABLE [dbo].[MerchantDetails] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [MerchantName]       VARCHAR (50)     NULL,
    [MerchantCardNumber] VARCHAR (18)     NULL,
    [ccv]                NUMERIC (3)      NULL,
    [issue_date]         DATETIME         NULL,
    [expiry_date]        DATETIME         NULL,
    CONSTRAINT [PK_MerchantDetails] PRIMARY KEY CLUSTERED ([Id] ASC)
);

