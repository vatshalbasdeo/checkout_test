﻿CREATE TABLE [dbo].[Transactions] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [TransactionNumber] NVARCHAR (50)    NULL,
    [MerchantId]        UNIQUEIDENTIFIER NULL,
    [UserId]            UNIQUEIDENTIFIER NULL,
    [Amount]            FLOAT (53)       NULL,
    [currency]          VARCHAR (50)     NULL,
    [datetimeCreated]   DATETIME         NULL,
    CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Transactions_MerchantDetails] FOREIGN KEY ([MerchantId]) REFERENCES [dbo].[MerchantDetails] ([Id]),
    CONSTRAINT [FK_Transactions_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);

