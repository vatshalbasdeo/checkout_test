﻿CREATE TABLE [dbo].[UserCardDetails] (
    [Id]         UNIQUEIDENTIFIER NOT NULL,
    [UserId]     UNIQUEIDENTIFIER NULL,
    [CardNumber] VARCHAR (18)     NULL,
    [ccv]        NUMERIC (3)      NULL,
    [issueDate]  DATETIME         NULL,
    [expiryDate] DATETIME         NULL,
    CONSTRAINT [PK_UserCarDetails] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserCardDetails_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([Id])
);

