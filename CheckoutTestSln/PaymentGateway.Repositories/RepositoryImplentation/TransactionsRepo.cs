﻿using PaymentGateway.Data;
using PaymentGateway.ServiceInterfacesModels.Interfaces;
using PaymentGateway.ServiceInterfacesModels.Models;
using PaymentGateway.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.Repositories.RepositoryImplentation
{
    public class TransactionsRepo : ITransaction
    {
        private readonly PaymentGatewayDbEntities _db;

        public TransactionsRepo(PaymentGatewayDbEntities db)
        {
            _db = db;
        }

        public bool AddTransaction(TransactionModel transaction)
        {
            try
            {
                var nextTraxNum = GetNextTransactionNumberForUser(transaction.UserId.Value);
                var newTransaction = new Transaction
                {
                    Id = Guid.NewGuid(),
                    MerchantId = transaction.MerchantId,
                    Amount = transaction.Amount,
                    currency = transaction.currency,
                    datetimeCreated = transaction.datetimeCreated,
                    UserId = transaction.UserId,
                    TransactionNumber= nextTraxNum.ToString(),
                    BankCr = transaction.BankCreditId,
                    BankDr =transaction.BankDebitId,
                    CustomerDetailsId = transaction.CustomerDetailId,
                    MerchantDetailsId = transaction.MerchantDetailId
                };
                _db.Transactions.Add(newTransaction);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

                new Logger().AddLogMessage(ex);
            }
            return false;
        }

        public List<TransactionListModel> GetAllMyTransactions(Guid userId)
        {
            List<TransactionListModel> transactionsList = new List<TransactionListModel>();
            try
            {
                transactionsList = (from trans in _db.Transactions
                                    join customer in _db.UserCardDetails on trans.CustomerDetailsId equals customer.Id
                                    join merchant in _db.UserCardDetails on trans.MerchantDetailsId equals merchant.Id

                                    where trans.UserId == userId

                                    select new TransactionListModel
                                    {
                                        Id = trans.Id,
                                        MerchantCardNumber = merchant.CardNumber.ToString(),
                                        CustomerCardNumber = customer.CardNumber.ToString(),
                                        Amount = trans.Amount,
                                        currency = trans.currency,
                                        datetimeCreated = trans.datetimeCreated,
                                        TransactionNumber = trans.TransactionNumber

                                    }
                       ).OrderBy(x=>x.datetimeCreated).ToList();
                return transactionsList;
            }
            catch (Exception ex)
            {

                new Logger().AddLogMessage(ex);
            }
            return transactionsList;
        }

        public int GetNextTransactionNumberForUser(Guid userId)
        {
            try
            {
                var lastItem = _db.Transactions.OrderByDescending(i => i.datetimeCreated).FirstOrDefault();
                if (lastItem != null)
                {
                    return Convert.ToInt32(lastItem.TransactionNumber) + 1;
                }
                else
                    return 1;
            }
            catch (Exception ex)
            {

                new Logger().AddLogMessage(ex);
            }
            return 0;
        }
    }
}
