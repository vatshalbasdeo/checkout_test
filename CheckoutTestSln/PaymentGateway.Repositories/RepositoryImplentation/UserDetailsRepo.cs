﻿using PaymentGateway.Data;
using PaymentGateway.ServiceInterfacesModels.Interfaces;
using PaymentGateway.ServiceInterfacesModels.Models;
using PaymentGateway.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.Repositories.RepositoryImplentation
{
    public class UserDetailsRepo : IUserDetails
    {
        private readonly PaymentGatewayDbEntities _db;

        public UserDetailsRepo(PaymentGatewayDbEntities db)
        {
            _db = db;
        }

        public bool AddUserDetails(UserDetails userCardDetail)
        {
            try
            {

                var userDetailsExist = _db.UserCardDetails.Where(i => i.CardNumber == userCardDetail.CardNumber).Any();
                if (!userDetailsExist)
                {
                    var userdetails = new UserCardDetail
                    {
                        Id = Guid.NewGuid(),
                        UserId = userCardDetail.UserId,
                        issueDate = userCardDetail.IssueDate,
                        expiryDate = userCardDetail.ExpiryDate,
                        CardNumber = userCardDetail.CardNumber,
                        ccv = userCardDetail.Ccv
                    };
                    _db.UserCardDetails.Add(userdetails);
                    _db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                new Logger().AddLogMessage(ex);
            }

            return false;
        }

        public bool DeleteUserDetails(UserDetails userCardDetail)
        {
            try
            {

                var userDetails = _db.UserCardDetails.Where(i => i.Id == userCardDetail.Id).SingleOrDefault();
                if (userDetails != null)
                {
                    _db.UserCardDetails.Remove(userDetails);
                    _db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                new Logger().AddLogMessage(ex);
            }

            return false;
        }

        public UserDetails GetUsersDetailsById(Guid userDetailId)
        {
            try
            {

                var userDetails = _db.UserCardDetails.Where(i => i.Id == userDetailId).Select(i => new UserDetails
                {
                    Id = i.Id,
                    CardNumber = i.CardNumber,
                    Ccv = i.ccv,
                    IssueDate = i.issueDate,
                    ExpiryDate = i.expiryDate,
                    UserId = i.UserId
                }).SingleOrDefault();
                return userDetails;
            }
            catch (Exception ex)
            {

                new Logger().AddLogMessage(ex);
            }
            return new UserDetails();
        }

        public List<UserDetails> GetUsersDetailsByUserId(Guid userId)
        {
            try
            {
                var userDetailsList = _db.UserCardDetails.Where(i => i.UserId == userId).Select(i => new UserDetails
                {
                    Id = i.Id,
                    CardNumber = i.CardNumber,
                    Ccv = i.ccv,
                    IssueDate = i.issueDate,
                    ExpiryDate = i.expiryDate,
                    UserId = i.UserId
                }).ToList();
                return userDetailsList;
            }
            catch (Exception ex)
            {

                new Logger().AddLogMessage(ex);
            }
            return new List<UserDetails>();

        }

        public bool ModifyUserDetails(UserDetails userCardDetail)
        {
            try
            {
                var userDetails = _db.UserCardDetails.Where(i => i.Id == userCardDetail.Id).SingleOrDefault();
                if (userDetails != null)
                {
                    userDetails.issueDate = userCardDetail.IssueDate;
                    userDetails.expiryDate = userCardDetail.ExpiryDate;
                    userDetails.CardNumber = userCardDetail.CardNumber;
                    userDetails.ccv = userCardDetail.Ccv;
                    _db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                new Logger().AddLogMessage(ex);
            }


            return false;
        }

        public UserDetails GetMainUsersDetailsByUserId(Guid userDetailId)
        {
            try
            {
                var userDetails = _db.UserCardDetails.Where(i => i.UserId == userDetailId).Select(i => new UserDetails
                {
                    Id = i.Id,
                    CardNumber = i.CardNumber,
                    Ccv = i.ccv,
                    IssueDate = i.issueDate,
                    ExpiryDate = i.expiryDate,
                    UserId = i.UserId
                }).FirstOrDefault();
                return userDetails;
            }
            catch (Exception ex)
            {

                new Logger().AddLogMessage(ex);
            }
            return new UserDetails();
        }
    }
}
