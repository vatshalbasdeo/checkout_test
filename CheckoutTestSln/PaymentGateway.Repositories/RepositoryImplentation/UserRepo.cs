﻿using PaymentGateway.Data;
using PaymentGateway.ServiceInterfacesModels.Interfaces;
using PaymentGateway.ServiceInterfacesModels.Models;
using PaymentGateway.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.Repositories.RepositoryImplentation
{
    public class UserRepo : IUserRepo
    {
        private readonly PaymentGatewayDbEntities _db;

        public UserRepo(PaymentGatewayDbEntities db)
        {
            _db = db;
        }

        public UserProcessResult AddUser(UserLogin newUser)
        {
            try
            {

                User user = new User
                {
                    Id = Guid.NewGuid(),
                    UserName = newUser.UserName,
                    Password = EncryptHelper.Encrypt(newUser.Password)
                };

                var userExist = _db.Users.Where(i => i.UserName.Equals(user.UserName)).Any();
                if (!userExist)
                {
                    _db.Users.Add(user);
                    _db.SaveChanges();
                    newUser.Id = user.Id;
                    return new UserProcessResult { Status = true, UserLogged = newUser };
                }
            }
            catch (Exception ex)
            {

                new Logger().AddLogMessage(ex);
            }
            return new UserProcessResult { Status = false };
        }

        public List<UserLogin> GetAllUsers()
        {
            try
            {
                var userList = _db.Users.Where(i => i.UserName != null).Select(i => new UserLogin
                {
                    Id = i.Id,
                    UserName = i.UserName,
                    Password = i.Password
                }).ToList();
                return userList;
            }
            catch (Exception ex)
            {
                new Logger().AddLogMessage(ex);
            }
            return new List<UserLogin>();
        }

        public User GetUsersById(Guid UserId)
        {
            try
            {

                var user = _db.Users.Where(i => i.Id == UserId).SingleOrDefault();
                return user;
            }
            catch (Exception ex)
            {

                new Logger().AddLogMessage(ex);
            }
            return new User();
        }

        public UserLogin Login(string username, string password)
        {
            try
            {

                var cryptedPassword = EncryptHelper.Encrypt("qwerty");
                var user = _db.Users.Where(i => i.UserName == username && i.Password == cryptedPassword).Select(i => new UserLogin
                {
                    Id = i.Id,
                    UserName = i.UserName,
                    Password = i.Password
                }).SingleOrDefault();
                return user;
            }
            catch (Exception ex)
            {

                new Logger().AddLogMessage(ex);
            }
            return new UserLogin();
        }

        public UserProcessResult ModifyUserPassword(UserLogin User)
        {
            try
            {

                var user = _db.Users.Where(i => i.Id == User.Id).SingleOrDefault();
                if (user != null)
                {
                    user.Password = EncryptHelper.Encrypt(User.Password);
                    _db.SaveChanges();
                    return new UserProcessResult { Status = true };
                }
            }
            catch (Exception ex)
            {

                new Logger().AddLogMessage(ex);
            }
            return new UserProcessResult { Status = false };
        }
    }
}
