﻿using PaymentGateway.ServiceInterfacesModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.ServiceInterfacesModels.Interfaces
{
    public interface ITransaction
    {
        int GetNextTransactionNumberForUser(Guid userId);
        bool AddTransaction(TransactionModel transaction);
        List<TransactionListModel> GetAllMyTransactions(Guid userId);
    }
}
