﻿using PaymentGateway.Data;
using PaymentGateway.ServiceInterfacesModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.ServiceInterfacesModels.Interfaces
{
    public interface IUserDetails
    {
        List<UserDetails> GetUsersDetailsByUserId(Guid userId);

        UserDetails GetUsersDetailsById(Guid userDetailId);

        bool ModifyUserDetails(UserDetails userCardDetail);

        bool AddUserDetails(UserDetails userCardDetail);

        bool DeleteUserDetails(UserDetails userCardDetail);

        UserDetails GetMainUsersDetailsByUserId(Guid userDetailId);
    }
}
