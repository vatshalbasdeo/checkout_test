﻿using PaymentGateway.Data;
using PaymentGateway.ServiceInterfacesModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.ServiceInterfacesModels.Interfaces
{
    public interface IUserRepo
    {
        List<UserLogin> GetAllUsers();
        User GetUsersById(Guid UserId);
        UserProcessResult AddUser(UserLogin User);
        UserProcessResult ModifyUserPassword(UserLogin User);
        UserLogin Login(string username, string password);
      
    }
}
