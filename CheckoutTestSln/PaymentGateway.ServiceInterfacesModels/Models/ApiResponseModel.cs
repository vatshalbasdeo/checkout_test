﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.ServiceInterfacesModels.Models
{
    public class ApiResponseModel
    {
        public ApiResponseModel()
        {
            Id = Guid.Empty;
            Status = false;

        }
        public Guid Id { get; set; }
        public bool Status { get; set; }

    }
}
