﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.ServiceInterfacesModels.Models
{
    public class PurchaseModel
    {
        public double amount { get; set; }
        public string currency { get; set; }
        public Guid MerchantId { get; set; }

    }
}
