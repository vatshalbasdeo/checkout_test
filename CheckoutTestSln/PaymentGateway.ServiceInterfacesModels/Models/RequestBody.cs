﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.ServiceInterfacesModels.Models
{
    public class RequestBody
    {
        public string cardNumber { get; set; }
        public decimal ccv { get; set; }
        public double amount { get; set; }
        public DateTime expiryDate { get; set; }

    }
}
