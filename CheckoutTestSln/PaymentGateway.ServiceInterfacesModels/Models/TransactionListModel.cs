﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.ServiceInterfacesModels.Models
{
    public class TransactionListModel
    {
            public Guid Id { get; set; }
            public string TransactionNumber { get; set; }
        private string _merchantCard { get; set; }
        public string MerchantCardNumber { get
            {
                var firstpart = _merchantCard.Substring(0,4);
                var lastpart = _merchantCard.Substring(_merchantCard.Length - 4);
                return firstpart+" xxxx xxxx " + lastpart;
            }
            set { _merchantCard = value; }
        }
        private string _customerCard { get; set; }
        public string CustomerCardNumber {
            get
            {
                var firstpart = _merchantCard.Substring(0, 4);
                var lastpart = _customerCard.Substring(_customerCard.Length - 4);
                return firstpart + " xxxx xxxx " + lastpart;
            }
            set { _customerCard = value; }
        }
            public double? Amount { get; set; }
            public string currency { get; set; }
            public DateTime? datetimeCreated { get; set; }

            
        
    }
}
