﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.ServiceInterfacesModels.Models
{
    public class TransactionModel
    {
        public Guid Id { get; set; }
        public string TransactionNumber { get; set; }
        public Guid? MerchantId { get; set; }
        public Guid? UserId { get; set; }
        public double? Amount { get; set; }
        public string currency { get; set; }
        public DateTime? datetimeCreated { get; set; }

        public Guid BankDebitId { get; set; }
        public Guid BankCreditId { get; set; }

        public Guid CustomerDetailId { get; set; }
        public Guid MerchantDetailId { get; set; }
    }
}
