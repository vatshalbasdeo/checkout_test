﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.ServiceInterfacesModels.Models
{
    public class UserDetails
    {
        public Guid Id { get; set; }
        public Guid? UserId { get; set; }
        public string CardNumber { get; set; }
        public decimal? Ccv { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
    }
}
