﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.ServiceInterfacesModels.Models
{
    public class UserProcessResult
    {
        public bool Status { get; set; }
        public UserLogin UserLogged { get; set; }

    }
}
