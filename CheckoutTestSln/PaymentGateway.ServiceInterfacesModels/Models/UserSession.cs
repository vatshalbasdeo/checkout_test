﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.ServiceInterfacesModels.Models
{
    public class UserSession
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }

        public string EncryptUserSession()
        {
            return this.Id.ToString() + ";" + this.UserName;
        }

        public UserSession DecryptUserSession(string str)
        {
            var arrStr = str.Split(';');

            return new UserSession { Id = Guid.Parse(arrStr[0]),UserName = arrStr[1]};
        }
    }
}
