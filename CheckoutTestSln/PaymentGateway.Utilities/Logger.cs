﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.Utilities
{
    public class Logger
    {
        private const string FILE_EXT = ".log";
        private readonly string datetimeFormat;
        private readonly string logFilename;
        private readonly string logFilepath;

        public Logger()
        {
            datetimeFormat = "yyyy-MM-dd HH:mm:ss.fff";
            //logFilename = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + FILE_EXT;
            logFilepath = "C:\\Users\\Public\\oddestoodds";
            logFilename = logFilepath + "\\Logs" + FILE_EXT;

            // Log file header line
            string logHeader = logFilename + " is created.";


            if (!Directory.Exists(logFilepath))
            {
                Directory.CreateDirectory(logFilepath);
            }

            if (!System.IO.File.Exists(logFilename))
            {
                WriteLine(System.DateTime.Now.ToString(datetimeFormat) + " " + logHeader, false);
            }
        }

        public void AddLogMessage(Exception ex)
        {
            var text = ex.GetType().FullName;
            text += "Message : " + ex.Message;
            text += "StackTrace : " + ex.StackTrace;
            WriteFormattedLog(LogLevel.ADDLOG, text);
        }

        private void WriteLine(string text, bool append = true)
        {
            try
            {
                using (System.IO.StreamWriter writer = new System.IO.StreamWriter(logFilename, append, System.Text.Encoding.UTF8))
                {
                    if (!string.IsNullOrEmpty(text))
                    {
                        writer.WriteLine(text);
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        private void WriteFormattedLog(LogLevel level, string text)
        {
            string pretext;
            switch (level)
            {
                case LogLevel.ADDLOG:
                    pretext = System.DateTime.Now.ToString(datetimeFormat) + " [ADDLOG]   ";
                    break;
                default:
                    pretext = "";
                    break;
            }

            WriteLine(pretext + text);
        }

        [System.Flags]
        private enum LogLevel
        {
            ADDLOG
        }
    }
}
