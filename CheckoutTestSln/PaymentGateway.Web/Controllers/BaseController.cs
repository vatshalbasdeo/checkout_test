﻿using PaymentGateway.ServiceInterfacesModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PaymentGateway.Web.Controllers
{
    public class BaseController : Controller
    {
        public UserSession UserSession {get;set;}

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            UserSession = (UserSession)(Session["UserSession"]);

            var controllerName = filterContext.RouteData.Values["controller"];
            if (!controllerName.Equals("Login"))
            {
               if(UserSession==null)
                {
                    filterContext.Result = new RedirectResult("/Login/Index");
                }
            }

            else
            {
                filterContext.Result = new RedirectResult("/Login/Index");
            }

            base.OnActionExecuting(filterContext);

        }
    }
}