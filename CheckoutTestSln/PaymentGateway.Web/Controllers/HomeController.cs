﻿using PaymentGateway.ServiceInterfacesModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PaymentGateway.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IUserDetails _userDetailsService;

        public HomeController(IUserDetails userDetailsService)
        {
            _userDetailsService = userDetailsService;
        }
   
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            var model = _userDetailsService.GetUsersDetailsByUserId(UserSession.Id);
            return View(model);
        }
    }
}
