﻿using PaymentGateway.ServiceInterfacesModels.Interfaces;
using PaymentGateway.ServiceInterfacesModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace PaymentGateway.Web.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LoginController : Controller
    {
        private readonly IUserRepo _userRepo;

        public LoginController(IUserRepo userRepo)
        {
            _userRepo = userRepo;
        }
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginUser(UserLogin userLogin)
        {
            var userLogged = _userRepo.Login(userLogin.UserName, userLogin.Password);
            if (userLogged == null)
            {
                return RedirectToAction("Index");
            }
            var userSession = new UserSession { Id = userLogged.Id, UserName = userLogged.UserName };
            Session["UserSession"] = userSession;
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RegisterUser(UserLogin user)
        {
            //Register User
            var userLogged = _userRepo.AddUser(user);

            if (userLogged.Status)
            {
                // login using new user
                var userSession = new UserSession { Id = userLogged.UserLogged.Id, UserName = userLogged.UserLogged.UserName };
                Session["UserSession"] = userSession;

            }

            // redirect to Home
            return RedirectToAction("Index", "Home");
        }

        public ActionResult LoginPurchase(double amount, string currency, string merchantid)
        {
            PurchaseModel purchaseInfo = new PurchaseModel { amount = amount, currency = currency, MerchantId = Guid.Parse(merchantid) };
            Session["purchaseInfo"] = purchaseInfo;

            return View("Index");
        }

        public ActionResult LogOut()
        {
            Session["UserSession"] = null;
            return RedirectToAction("Index");


        }

    }
}