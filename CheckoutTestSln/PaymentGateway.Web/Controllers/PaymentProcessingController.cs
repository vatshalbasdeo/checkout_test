﻿using PaymentGateway.ServiceInterfacesModels.Interfaces;
using PaymentGateway.ServiceInterfacesModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PaymentGateway.Web.Controllers
{
    public class PaymentProcessingController : BaseController
    {
        readonly IUserDetails _userDetails;
        readonly ITransaction _transactionService;

        public PaymentProcessingController(IUserDetails userDetails, ITransaction transaction)
        {
            _userDetails = userDetails;
            _transactionService = transaction;
        }

        // GET: PaymentProcessing
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult ProceedToPayment(Guid id)
        {
            var result = "Process Failed";
            var clientCardIdChosen = id;
            // Purchase model
            var purchaseModel = (PurchaseModel)Session["purchaseInfo"];
            
            // get customer details
            var customerDetails = _userDetails.GetUsersDetailsById(clientCardIdChosen);

            var deductionBody = new RequestBody
            {
                cardNumber =customerDetails.CardNumber,
                ccv =customerDetails.Ccv.Value,
                amount = purchaseModel.amount,
                expiryDate =customerDetails.ExpiryDate.Value
            };
            //process deduction customer
            var deduction = TaskDeductClient(deductionBody);
            if (deduction.Status)
            {
                // get merchant details 
                var merchantDetails = _userDetails.GetMainUsersDetailsByUserId(purchaseModel.MerchantId);

                var depositBody = new RequestBody
                {
                    cardNumber = merchantDetails.CardNumber,
                    ccv = merchantDetails.Ccv.Value,
                    amount = purchaseModel.amount,
                    expiryDate = merchantDetails.ExpiryDate.Value
                };
                // process addition of money merchant 
                var addition = TaskAdditionMerchant(depositBody);

                if (addition.Status && deduction.Status)
                {
                    //add transaction details to Db 
                    var transaction = new TransactionModel
                    {
                        UserId = UserSession.Id,
                        MerchantId = purchaseModel.MerchantId,
                        Amount = purchaseModel.amount,
                        currency = purchaseModel.currency,
                        datetimeCreated = DateTime.Now,

                        BankCreditId = addition.Id,
                        BankDebitId = deduction.Id,

                        CustomerDetailId = customerDetails.Id,
                        MerchantDetailId=merchantDetails.Id

                    };
                    _transactionService.AddTransaction(transaction);
                    result = "Process Completed Successfull ";
                }
                else { result = "Deduction was successfull but not deposit"; }
            }
            ViewBag.result = result;
            Session["purchaseInfo"] = null;
            return View();
        }

        public ApiResponseModel TaskDeductClient(RequestBody requestBody)
        {
            ApiResponseModel apiResponse = new ApiResponseModel();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56834/api/");
                //HTTP GET
                var responseTask = client.PostAsJsonAsync("BankDeduction",requestBody);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<ApiResponseModel>();
                    readTask.Wait();

                    apiResponse = readTask.Result;

                }
                else //web api sent error response 
                {
                    //log response status here..

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return apiResponse;

        }

        public ApiResponseModel TaskAdditionMerchant(RequestBody requestBody)
        {
            ApiResponseModel apiResponse = new ApiResponseModel();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56834/api/");
                //HTTP GET
                var responseTask = client.PostAsJsonAsync("BankDeposits",requestBody);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<ApiResponseModel>();
                    readTask.Wait();

                    apiResponse = readTask.Result;

                }
                else //web api sent error response 
                {
                    //log response status here..

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return apiResponse;

        }
    }
}