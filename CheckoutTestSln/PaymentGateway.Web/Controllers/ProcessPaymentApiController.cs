﻿using PaymentGateway.ServiceInterfacesModels.Interfaces;
using PaymentGateway.ServiceInterfacesModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;
using System.Web.Script.Serialization;

namespace PaymentGateway.Web.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProcessPaymentApiController : System.Web.Mvc.Controller
    {
      
        private readonly IUserRepo _userRepo;
        private readonly IUserDetails _userDetailsService;
        readonly ITransaction _transactionService;


        public ProcessPaymentApiController(IUserRepo userRepo, IUserDetails userDetailsService, ITransaction transaction)
        {
            _userRepo = userRepo;
            _userDetailsService = userDetailsService;
            _transactionService = transaction;
        }

        public string Get()
        {
            return "True";
        }

        public string Post(double amount, string currency, string merchantid, string userName, string password)
        {
            var serializer = new JavaScriptSerializer();
            string json = serializer.Serialize(new ApiResponseModel {Status=false });

            PurchaseModel purchaseInfo = new PurchaseModel { amount = amount, currency = currency, MerchantId = Guid.Parse(merchantid) };


            var userLogged = _userRepo.Login(userName, password);
            if (userLogged == null)
            {
                return json;
            }
            else
            {
                var userSession = new UserSession { Id = userLogged.Id, UserName = userLogged.UserName };


                var cardFirst = _userDetailsService.GetUsersDetailsByUserId(userSession.Id).FirstOrDefault();
                if (cardFirst.Id != Guid.Empty)
                {
                    var resultProcess = ProcessPayment(cardFirst.Id, purchaseInfo, userSession);
                    return serializer.Serialize(resultProcess);
                }

            }

            return json; 
        }

        private ApiResponseModel ProcessPayment(Guid clientId, PurchaseModel purchaseInfo, UserSession userSession)
        {
            ApiResponseModel result = new ApiResponseModel();
            var clientCardIdChosen = clientId;
            // Purchase model
            var purchaseModel = purchaseInfo;

            // get customer details
            var customerDetails = _userDetailsService.GetUsersDetailsById(clientCardIdChosen);

            var deductionBody = new RequestBody
            {
                cardNumber = customerDetails.CardNumber,
                ccv = customerDetails.Ccv.Value,
                amount = purchaseModel.amount,
                expiryDate = customerDetails.ExpiryDate.Value
            };
            //process deduction customer
            var deduction = TaskDeductClient(deductionBody);
            if (deduction.Status)
            {
                // get merchant details 
                var merchantDetails = _userDetailsService.GetMainUsersDetailsByUserId(purchaseModel.MerchantId);

                var depositBody = new RequestBody
                {
                    cardNumber = merchantDetails.CardNumber,
                    ccv = merchantDetails.Ccv.Value,
                    amount = purchaseModel.amount,
                    expiryDate = merchantDetails.ExpiryDate.Value
                };
                // process addition of money merchant 
                var addition = TaskAdditionMerchant(depositBody);

                if (addition.Status && deduction.Status)
                {
                    //add transaction details to Db 
                    var transaction = new TransactionModel
                    {
                        UserId = userSession.Id,
                        MerchantId = purchaseModel.MerchantId,
                        Amount = purchaseModel.amount,
                        currency = purchaseModel.currency,
                        datetimeCreated = DateTime.Now,

                        BankCreditId = addition.Id,
                        BankDebitId = deduction.Id,

                        CustomerDetailId = customerDetails.Id,
                        MerchantDetailId = merchantDetails.Id

                    };
                    _transactionService.AddTransaction(transaction);
                    result.Status = true;
                }
                else { result.Status = false; }
            }
            return result;
        }

        private ApiResponseModel TaskDeductClient(RequestBody requestBody)
        {
            ApiResponseModel apiResponse = new ApiResponseModel();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56834/api/");
                //HTTP GET
                var responseTask = client.PostAsJsonAsync("BankDeduction", requestBody);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<ApiResponseModel>();
                    readTask.Wait();

                    apiResponse = readTask.Result;

                }
                else //web api sent error response 
                {
                    //log response status here..

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return apiResponse;

        }

        private ApiResponseModel TaskAdditionMerchant(RequestBody requestBody)
        {
            ApiResponseModel apiResponse = new ApiResponseModel();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56834/api/");
                //HTTP GET
                var responseTask = client.PostAsJsonAsync("BankDeposits", requestBody);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<ApiResponseModel>();
                    readTask.Wait();

                    apiResponse = readTask.Result;

                }
                else //web api sent error response 
                {
                    //log response status here..

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return apiResponse;

        }






    }
}
