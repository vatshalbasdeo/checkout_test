﻿using PaymentGateway.ServiceInterfacesModels.Interfaces;
using System;
using System.Web.Mvc;

namespace PaymentGateway.Web.Controllers
{
    public class TrasactionListController : BaseController
    {
        private readonly IUserDetails _userDetails;
        readonly ITransaction _transactionService;

        public TrasactionListController(IUserDetails userDetails, ITransaction transaction)
        {
            _userDetails = userDetails;
            _transactionService = transaction;
        }
       
        public ActionResult Index()
        {
            var result = _transactionService.GetAllMyTransactions(UserSession.Id);
            return View(result);
        }

    }
}
