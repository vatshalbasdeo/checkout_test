﻿using PaymentGateway.ServiceInterfacesModels.Interfaces;
using PaymentGateway.ServiceInterfacesModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PaymentGateway.Web.Controllers
{
    public class UserDetailsController : BaseController
    {
        private readonly IUserDetails _userDetailsService;

        public UserDetailsController(IUserDetails userDetailsService)
        {
            _userDetailsService = userDetailsService;
        }

        // GET: UserDetails
        public ActionResult Index()
        {
            var model = _userDetailsService.GetUsersDetailsByUserId(UserSession.Id);
            return View(model);
        }

        // GET: UserDetails/Details/5
        public ActionResult Details(Guid id)
        {
            var model = _userDetailsService.GetUsersDetailsById(id);
            return View(model);
        }

        // GET: UserDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserDetails/Create
        [HttpPost]
        public ActionResult Create(UserDetails collection)
        {
            try
            {
                collection.UserId = UserSession.Id;
                // TODO: Add insert logic here
                var result = _userDetailsService.AddUserDetails(collection);
                if (result)
                    return RedirectToAction("Index");
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: UserDetails/Edit/5
        public ActionResult Edit(Guid id)
        {
            var model = _userDetailsService.GetUsersDetailsById(id);
            return View(model);
        }

        // POST: UserDetails/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid id, UserDetails collection)
        {
            try
            {
                // TODO: Add update logic here
                var result = _userDetailsService.ModifyUserDetails(collection);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: UserDetails/Delete/5
        public ActionResult Delete(Guid id)
        {
            var model = _userDetailsService.GetUsersDetailsById(id);
            return View();
        }

        // POST: UserDetails/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, UserDetails collection)
        {
            try
            {
                // TODO: Add delete logic here
                var result = _userDetailsService.DeleteUserDetails(collection);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
